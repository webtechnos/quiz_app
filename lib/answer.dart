import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function _handler;
  final String _answerText;

  Answer(this._answerText, this._handler);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Colors.lightBlueAccent,
        textColor: Colors.black,
        child: Text(_answerText),
        onPressed: _handler,
      ),
    );
  }
}
