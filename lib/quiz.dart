import 'package:flutter/material.dart';

import './question.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final String questionText;
  final List<Map<String, dynamic>> answers;
  final Function otvet;

  Quiz({this.questionText, this.answers, this.otvet});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Question(questionText),
        ...answers
            .map((answer) =>
                Answer(answer['text'], () => otvet(answer['score'])))
            .toList()
      ],
    );
  }
}
