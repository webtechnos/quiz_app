import 'package:flutter/material.dart';
import 'package:quiz_app/answer.dart';
import 'package:quiz_app/question.dart';

import 'quiz.dart';
import 'result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _questionsIndex = 0;
  int _totalScore = 0;

  final List questions = [
    {
      'questionText': 'Вопрос номер 1',
      'answers': [
        {'text': 'Ответ 1', 'score': 10},
        {'text': 'Ответ 2', 'score': 5},
        {'text': 'Ответ 3', 'score': 1},
      ]
    },
    {
      'questionText': 'Вопрос номер 2',
      'answers': [
        {'text': 'Ответ 4', 'score': 4},
        {'text': 'Ответ 5', 'score': 3},
        {'text': 'Ответ 6', 'score': 2},
      ]
    },
    {
      'questionText': 'Вопрос номер 3',
      'answers': [
        {'text': 'Ответ 7', 'score': 1},
        {'text': 'Ответ 8', 'score': 1},
        {'text': 'Ответ 9', 'score': 1},
      ]
    }
  ];

  void _otvet(int score) {
    _totalScore += score;
    print(_questionsIndex < questions.length
        ? 'otvet .. ($_questionsIndex) ddd - ${questions[_questionsIndex]}'
        : 'none');
    print(_totalScore);
    setState(() {
      _questionsIndex++;
    });
  }

  void _resetQuiz() {
    _totalScore = 0;
    setState(() {
      _questionsIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('tiitle'),
        ),
        body: _questionsIndex < questions.length
            ? Quiz(
                questionText: questions[_questionsIndex]['questionText'],
                answers: questions[_questionsIndex]['answers'],
                otvet: _otvet,
              )
            : Result(
                _totalScore,
                _resetQuiz,
              ),
      ),
    );
  }
}
